Super Flat Remix GNOME theme
===========

This theme is available for use under a Creative Commons ShareAlike license.

Based on [this theme](https://github.com/daniruiz/Super-Flat-Remix-GNOME-theme) <br />
You can download it [here](http://gnome-look.org/content/show.php/Super+Flat?content=174699) <br />

![alt tag](Screenshot1.png)
![alt tag](Screenshot2.png)
![alt tag](Screenshot3.jpg)
